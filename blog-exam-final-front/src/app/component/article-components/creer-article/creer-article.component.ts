import {Component, Input, OnInit} from '@angular/core';
import {BlogApiService} from '../../../service/BlogApi.service';
import {UtilisateurModel} from '../../../model/utilisateur.model';
import {ArticleModel} from '../../../model/article.model';

@Component({
  selector: 'app-creer-article',
  templateUrl: './creer-article.component.html',
  styleUrls: ['./creer-article.component.css']
})
export class CreerArticleComponent implements OnInit {
  @Input() private pseudo: string;
  @Input() private articles: ArticleModel;
  private utilisateur: UtilisateurModel;
  private titreSaisie: string;
  private resumeSaisie: string;
  private contenuSaisi: string;
  private categorieSaisi: string;
  private listeUtilisateurs: UtilisateurModel[] = [];

  constructor(private blogApiService: BlogApiService) {
  }

  ngOnInit() {
    this.blogApiService.getUtilisateurApi().subscribe(utilisateurApi => {
      this.listeUtilisateurs = utilisateurApi;
    });
  }


  enregistrer() {
    const newSaisieArticle = new ArticleModel(this.titreSaisie, this.resumeSaisie, this.contenuSaisi, this.utilisateur, this.categorieSaisi);
    this.blogApiService.postArticleApi(newSaisieArticle).subscribe();
  }

  getUtilisateur() {
    return this.utilisateur;
  }

}
