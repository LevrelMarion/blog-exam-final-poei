import {Component, Input, OnInit} from '@angular/core';
import {ArticleModel} from '../../../model/article.model';
import {BlogApiService} from '../../../service/BlogApi.service';

@Component({
  selector: 'app-show-articles',
  templateUrl: './show-articles.component.html',
  styleUrls: ['./show-articles.component.css']
})
export class ShowArticlesComponent implements OnInit {

  @Input() articles: ArticleModel;
  show: boolean = false;


  constructor(private blogApiService: BlogApiService) {
  }

  ngOnInit() {

  }


}
