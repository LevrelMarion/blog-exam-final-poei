import { Component, OnInit } from '@angular/core';
import {ArticleModel} from '../../../model/article.model';
import {BlogApiService} from '../../../service/BlogApi.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  listeArticles: ArticleModel[] = [];


  constructor(private blogApiService: BlogApiService) {
  }

  ngOnInit() {
    this.blogApiService.getArticleApi().subscribe(articleApi => {
      this.listeArticles = articleApi;
    });
  }

}
