import {Component, Input, OnInit} from '@angular/core';
import {BlogApiService} from '../../../service/BlogApi.service';
import {NewUtilisateurModel} from '../../../model/newUtilisateur.model';

@Component({
  selector: 'app-creer-utilisateur',
  templateUrl: './creer-utilisateur.component.html',
  styleUrls: ['./creer-utilisateur.component.css']
})
export class CreerUtilisateurComponent implements OnInit {
  pseudoSaisi: string;
  mailSaisi: string;


  constructor(private blogApiService: BlogApiService) { }

  ngOnInit() {
  }

  enregistrer() {
    const newSaisieUtilisateur = new NewUtilisateurModel(this.pseudoSaisi, this.mailSaisi);
    this.blogApiService.postUtilisateurApi(newSaisieUtilisateur).subscribe();
  }
}
