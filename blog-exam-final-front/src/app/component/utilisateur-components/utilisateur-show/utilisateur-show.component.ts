import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UtilisateurModel} from '../../../model/utilisateur.model';
import {BlogApiService} from '../../../service/BlogApi.service';

@Component({
  selector: 'app-utilisateur-show',
  templateUrl: './utilisateur-show.component.html',
  styleUrls: ['./utilisateur-show.component.css']
})
export class UtilisateurShowComponent implements OnInit {

  @Input() utilisateur: UtilisateurModel;
  @Output() choixUtilisateur: string;


  constructor(private blogApiService: BlogApiService ) {

  }

  ngOnInit() {
  }

  getChoixUtilisateur() {
    console.log(this.choixUtilisateur = this.utilisateur.pseudo);
    return this.choixUtilisateur = this.utilisateur.pseudo;
  }

}
