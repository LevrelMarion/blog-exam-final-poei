import {Component, Input, OnInit, Output} from '@angular/core';
import {UtilisateurModel} from '../../../model/utilisateur.model';
import {BlogApiService} from '../../../service/BlogApi.service';

@Component({
  selector: 'app-utilisateur-fetch',
  templateUrl: './utilisateur-fetch.component.html',
  styleUrls: ['./utilisateur-fetch.component.css']
})
export class UtilisateurFetchComponent implements OnInit {
  listeUtilisateurs: UtilisateurModel[] = [];

  constructor(private blogApiService: BlogApiService) { }

  ngOnInit() {
    this.blogApiService.getUtilisateurApi().subscribe(utilisateurApi => {
      this.listeUtilisateurs = utilisateurApi;
    });
  }

}
