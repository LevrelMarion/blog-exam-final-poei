import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {BlogApiService} from './service/BlogApi.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import { HomeComponent } from './component/home-components/home/home.component';
import { ArticleComponent } from './component/article-components/article/article.component';
import { ShowArticlesComponent } from './component/article-components/show-articles/show-articles.component';
import { UtilisateurFetchComponent } from './component/utilisateur-components/utilisateur-fetch/utilisateur-fetch.component';
import { UtilisateurShowComponent } from './component/utilisateur-components/utilisateur-show/utilisateur-show.component';
import { HomeChoixComponent } from './component/home-components/home-choix/home-choix.component';
import { CreerUtilisateurComponent } from './component/utilisateur-components/creer-utilisateur/creer-utilisateur.component';
import { ChoixComponent } from './component/utilisateur-components/choix/choix.component';
import { CreerArticleComponent } from './component/article-components/creer-article/creer-article.component';

const appRoutes: Routes = [
  { path: 'articles', component:  ArticleComponent } ,
  { path: '', component:  HomeChoixComponent } ,
  { path: 'utilisateurs', component:  UtilisateurFetchComponent } ,
  { path: 'creer-utilisateurs', component: CreerUtilisateurComponent } ,
  { path: 'creer-article', component: CreerArticleComponent } ,


];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ArticleComponent,
    ShowArticlesComponent,
    UtilisateurFetchComponent,
    UtilisateurShowComponent,
    HomeChoixComponent,
    CreerUtilisateurComponent,
    ChoixComponent,
    CreerArticleComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes),
  ],
  providers: [
    BlogApiService

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
