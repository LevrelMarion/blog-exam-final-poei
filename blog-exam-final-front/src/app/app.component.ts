import { Component } from '@angular/core';
import {BlogApiService} from './service/BlogApi.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'exam-final-poei-front';

}
