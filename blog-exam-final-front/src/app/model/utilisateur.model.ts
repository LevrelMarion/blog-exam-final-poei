export class UtilisateurModel {
  id: string;
  pseudo: string;
  email: string;


  constructor(id: string, pseudo: string, email: string) {
    this.id = id;
    this.pseudo = pseudo;
    this.email = email;
  }
}
