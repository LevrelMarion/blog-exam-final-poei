export class NewUtilisateurModel {
  pseudo: string;
  email: string;


  constructor(pseudo: string, email: string) {
    this.pseudo = pseudo;
    this.email = email;
  }
}
