import {UtilisateurModel} from './utilisateur.model';

export class ArticleModel {
  titre: string;
  contenu: string;
  resume: string;
  utilisateur: UtilisateurModel;
  categories: string;


  constructor(titre: string, contenu: string, resume: string, utilisateur: UtilisateurModel, categories: string) {
    this.titre = titre;
    this.contenu = contenu;
    this.resume = resume;
    this.utilisateur = utilisateur;
    this.categories = categories;
  }
}
