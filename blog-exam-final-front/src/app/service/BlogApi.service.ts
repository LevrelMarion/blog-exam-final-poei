import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ArticleModel} from '../model/article.model';
import {UtilisateurModel} from '../model/utilisateur.model';
import {NewUtilisateurModel} from '../model/newUtilisateur.model';

@Injectable()
export class BlogApiService {

  constructor(
    private http: HttpClient
  ) {
  }

  getArticleApi(): Observable<ArticleModel[]> {
    return this.http.get<ArticleModel[]>('http://localhost:8080/articles');
  }
/*  getOneArticleApi(id: string): Observable<ArticleModel[]> {
    return this.http.get<ArticleModel[]>(`http://localhost:8080/articles/${id}`);
  }*/
  postArticleApi(bodyArticle: ArticleModel): Observable<ArticleModel> {
    return this.http.post<ArticleModel>(`http://localhost:8080/articles`, bodyArticle);
  }


  getUtilisateurApi(): Observable<UtilisateurModel[]> {
    return this.http.get<UtilisateurModel[]>('http://localhost:8080/utilisateurs');
  }
  postUtilisateurApi(bodyUtilisateur: NewUtilisateurModel): Observable<NewUtilisateurModel> {
    return this.http.post<NewUtilisateurModel>(`http://localhost:8080/utilisateurs`, bodyUtilisateur);
  }
}
