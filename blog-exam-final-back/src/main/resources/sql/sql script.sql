create table articles
(
id text primary key not null,
titre text not null,
contenu text not null,
resume varchar(150) not null,
categorie text not null,
utilisateur text not null,
FOREIGN KEY (utilisateur) REFERENCES utilisateurs(id)
);

create table utilisateurs
(
id text primary key not null,
pseudo text not null,
email text not null,
);


INSERT INTO articles(id, titre, contenu, resume)
VALUES
('1','la fin du monde','C"était à prévoir, les riches font face à l"épidémie mondiale de coronavirus de façon bien différente ' ||
                       '(et disproportionnée?) que le commun des mortel·les. Le New York Times raconte comment, aux États-Unis, ' ||
                       'certaines grandes fortunes tentent par tous les moyens de se protéger du virus et surtout des plus pauvres.' ||
                       'Équipements sophistiqués, jets privés, salles d"urgence médicale VIP et bunkers de fin du monde… ' ||
                       'Tout est envisagé. La paranoïa globale a d"abord débuté par des masques, tous plus élaborés ' ||
                       'et onéreux les uns que les autres, comme celui à 61 euros de Gwyneth Paltrow sur Instagram. ' ||
                       'Rappelons au passage que ces masques sont bien souvent inutiles face au coronavirus.',
 'Sommes-nous vraiment égaux face à de telles épidémies?');