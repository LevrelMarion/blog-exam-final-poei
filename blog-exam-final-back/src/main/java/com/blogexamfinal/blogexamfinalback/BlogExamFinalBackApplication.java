package com.blogexamfinal.blogexamfinalback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogExamFinalBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogExamFinalBackApplication.class, args);
	}

}
