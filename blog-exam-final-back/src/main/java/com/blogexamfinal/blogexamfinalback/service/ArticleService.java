package com.blogexamfinal.blogexamfinalback.service;

import com.blogexamfinal.blogexamfinalback.controllers.articleRepresentations.ArticleRepresentation;
import com.blogexamfinal.blogexamfinalback.controllers.articleRepresentations.NewArticleRepresenation;
import com.blogexamfinal.blogexamfinalback.domain.Article;
import com.blogexamfinal.blogexamfinalback.domain.Categories;
import com.blogexamfinal.blogexamfinalback.domain.Utilisateur;
import com.blogexamfinal.blogexamfinalback.repositories.ArticleRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ArticleService {

        private ArticleRepository articleRepository;
        private IdGenerator idGenerator;

    public ArticleService(ArticleRepository articleRepository, IdGenerator idGenerator) {
        this.articleRepository = articleRepository;
        this.idGenerator = idGenerator;
    }

    public List<Article> getAllArticles() {
        return (List<Article>) this.articleRepository.findAll();
    }

    public Optional<Article> getOneArticle(String id) {
        return this.articleRepository.findById(id);
    }

    public void deleteOneArticle(String id) {
       this.articleRepository.deleteById(id);
    }

    public Article createOneArticle (NewArticleRepresenation body) {
        Article article = new Article(this.idGenerator.generateNewId(), body.getTitre(),
                body.getContenu(), body.getResume(), body.getUtilisateur(), body.getCategories());
        return this.articleRepository.save(article);
    }

    public Article modifyOneArticle (String id, ArticleRepresentation body) {
        Article articleToModify = articleRepository.findById(id).orElseThrow();
        Article articleInBase = new Article(id, body.getTitre(), body.getContenu(), body.getResume(), body.getUtilisateur(), body.getCategories());
        String newTitre = "";
        String newContenu = "";
        String newResume = "";
        Categories changeCategorie = null;
        Utilisateur newIdUtilisateur = null;
  ;
        if (articleInBase.getTitre().equals("")) {
            newTitre = articleToModify.getTitre();
        } else {
            newTitre = articleInBase.getTitre();
        }
        if (articleInBase.getContenu().equals("")) {
            newContenu = articleToModify.getContenu();
        } else {
            newContenu = articleInBase.getContenu();
        }
        if (articleInBase.getResume().equals("")) {
            newResume = articleToModify.getResume();
        } else {
            newResume = articleInBase.getResume();
        }
        if (articleInBase.getCategories().equals(null)) {
            changeCategorie = articleToModify.getCategories();
        } else {
            changeCategorie = articleInBase.getCategories();
        }
       if (articleInBase.getUtilisateur().equals(null)) {
            newIdUtilisateur = articleToModify.getUtilisateur();
        } else {
            newIdUtilisateur = articleInBase.getUtilisateur();
        }
        Article newArticle = new Article(id, newTitre, newContenu, newResume, newIdUtilisateur, changeCategorie);
        return this.articleRepository.save(newArticle);

    }
}