package com.blogexamfinal.blogexamfinalback.service;

import com.blogexamfinal.blogexamfinalback.controllers.utilisateursRepresentations.NewUtilisateurRepresentation;
import com.blogexamfinal.blogexamfinalback.controllers.utilisateursRepresentations.UtilisateurRepresentation;
import com.blogexamfinal.blogexamfinalback.domain.Utilisateur;
import com.blogexamfinal.blogexamfinalback.repositories.UtilisateurRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class UtilisateurService {

    private UtilisateurRepository utilisateurRepository;
    private IdGenerator idGenerator;

    public UtilisateurService(UtilisateurRepository utilisateurRepository, IdGenerator idGenerator) {
        this.utilisateurRepository = utilisateurRepository;
        this.idGenerator = idGenerator;
    }

    public List<Utilisateur> getAllUtilisateurs() {
        return (List<Utilisateur>) utilisateurRepository.findAll();
    }

    public Optional<Utilisateur> getOneUtilisateur(String id) {
        return this.utilisateurRepository.findById(id);
    }

    public void deleteOneUtilisateur(String id) {
        this.utilisateurRepository.deleteById(id);
    }

    public Utilisateur createOneUtilisateur(NewUtilisateurRepresentation body) {
             Utilisateur utilisateur = new Utilisateur(this.idGenerator.generateNewId(),
                     body.getPseudo(), body.getEmail());
            return this.utilisateurRepository.save(utilisateur);
        }

    public Utilisateur modifyOneUtilisateur(String id, UtilisateurRepresentation body) {
        Utilisateur utilisateurToModify = utilisateurRepository.findById(id).orElseThrow();
        Utilisateur utilisateurInBase = new Utilisateur(id, body.getPseudo(), body.getEmail());
        String newPseudo = "";
        String newEmail = "";
       // List<Article> newIdArticle = null;

        if (utilisateurInBase.getPseudo().equals("")) {
            newPseudo = utilisateurToModify.getPseudo();
        } else {
            newPseudo =utilisateurInBase.getPseudo();
        }
        if (utilisateurInBase.getEmail().equals("")) {
            newEmail = utilisateurToModify.getEmail();
        } else {
            newEmail = utilisateurInBase.getEmail();
        }
        /* if (utilisateurInBase.getArticle().equals(null)) {
            newIdArticle = utilisateurToModify.getArticle();
        } else {
            newIdArticle = utilisateurInBase.getArticle();
        }*/
        Utilisateur newUtilisateur = new Utilisateur(id, newPseudo, newEmail);
        return this.utilisateurRepository.save(newUtilisateur);
    }
}