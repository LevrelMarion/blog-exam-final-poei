package com.blogexamfinal.blogexamfinalback.controllers.articleRepresentations;

import com.blogexamfinal.blogexamfinalback.domain.Categories;
import com.blogexamfinal.blogexamfinalback.domain.Utilisateur;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class NewArticleRepresenation {
    private String titre;
    private String contenu;
    private String resume;
    private Utilisateur utilisateur;
    private Categories categories;


    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

     public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Categories getCategories() {
        return categories;
    }

    public void setCategories(Categories categories) {
        this.categories = categories;
    }
}