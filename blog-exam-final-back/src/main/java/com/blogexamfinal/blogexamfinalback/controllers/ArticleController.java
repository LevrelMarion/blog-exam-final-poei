package com.blogexamfinal.blogexamfinalback.controllers;

import com.blogexamfinal.blogexamfinalback.controllers.articleRepresentations.NewArticleRepresenation;
import com.blogexamfinal.blogexamfinalback.controllers.articleRepresentations.ArticleRepresentation;
import com.blogexamfinal.blogexamfinalback.controllers.articleRepresentations.ArticleRepresentationMapper;
import com.blogexamfinal.blogexamfinalback.domain.Article;
import com.blogexamfinal.blogexamfinalback.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/articles")
public class ArticleController {

    private ArticleService articleService;
    private ArticleRepresentationMapper articleRepresentationMapper;

    @Autowired
    public ArticleController(ArticleService articleService, ArticleRepresentationMapper articleRepresentationMapper) {
        this.articleService = articleService;
        this.articleRepresentationMapper = articleRepresentationMapper;
    }

    @GetMapping
    List<ArticleRepresentation> getAllArticles() {
        return this.articleService.getAllArticles().stream()
                .map(this.articleRepresentationMapper::mapToDisplayableArticle)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    ResponseEntity<ArticleRepresentation> getOneArticle(@PathVariable("id") String id) {
        Optional<Article> article = this.articleService.getOneArticle(id);
        return article
                .map(this.articleRepresentationMapper::mapToDisplayableArticle)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ArticleRepresentation> deleteOneArticle(@PathVariable("id") String id) {
       this.articleService.deleteOneArticle(id);
       return ResponseEntity.noContent().build();
    }

    @PostMapping
    public ArticleRepresentation createArticle(@RequestBody NewArticleRepresenation body) {
        return articleRepresentationMapper.mapToDisplayableArticle(this.articleService.createOneArticle(body));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ArticleRepresentation> modifyArticle(@PathVariable("id") String id ,
                                                               @RequestBody ArticleRepresentation body) {
        this.articleService.modifyOneArticle(id, body);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
