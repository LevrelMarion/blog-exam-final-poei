package com.blogexamfinal.blogexamfinalback.controllers.utilisateursRepresentations;

import com.blogexamfinal.blogexamfinalback.domain.Utilisateur;
import org.springframework.stereotype.Component;

@Component
public class UtilisateurRepresentationMapper {
    public UtilisateurRepresentation mapToDisplayableUtilisateur(Utilisateur utilisateur) {
        UtilisateurRepresentation result = new UtilisateurRepresentation();
        result.setId(utilisateur.getId());
        result.setPseudo(utilisateur.getPseudo());
        result.setEmail(utilisateur.getEmail());
        // result.setArticle(utilisateur.getArticle());
        return result;
    }
}
