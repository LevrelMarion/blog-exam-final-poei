package com.blogexamfinal.blogexamfinalback.controllers.articleRepresentations;

import com.blogexamfinal.blogexamfinalback.domain.Article;
import org.springframework.stereotype.Component;

@Component
public class ArticleRepresentationMapper {
    public ArticleRepresentation mapToDisplayableArticle(Article article) {
        ArticleRepresentation result = new ArticleRepresentation();
        result.setId(article.getId());
        result.setTitre(article.getTitre());
        result.setContenu(article.getContenu());
        result.setResume(article.getResume());
        result.setUtilisateur(article.getUtilisateur());
        result.setCategories(article.getCategories());
        return result;
    }
}
