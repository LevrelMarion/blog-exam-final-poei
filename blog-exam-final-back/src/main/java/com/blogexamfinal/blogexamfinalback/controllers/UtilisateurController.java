package com.blogexamfinal.blogexamfinalback.controllers;

import com.blogexamfinal.blogexamfinalback.controllers.articleRepresentations.ArticleRepresentation;
import com.blogexamfinal.blogexamfinalback.controllers.articleRepresentations.ArticleRepresentationMapper;
import com.blogexamfinal.blogexamfinalback.controllers.articleRepresentations.NewArticleRepresenation;
import com.blogexamfinal.blogexamfinalback.controllers.utilisateursRepresentations.NewUtilisateurRepresentation;
import com.blogexamfinal.blogexamfinalback.controllers.utilisateursRepresentations.UtilisateurRepresentation;
import com.blogexamfinal.blogexamfinalback.controllers.utilisateursRepresentations.UtilisateurRepresentationMapper;
import com.blogexamfinal.blogexamfinalback.domain.Article;
import com.blogexamfinal.blogexamfinalback.domain.Utilisateur;
import com.blogexamfinal.blogexamfinalback.service.ArticleService;
import com.blogexamfinal.blogexamfinalback.service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/utilisateurs")
public class UtilisateurController {

    private UtilisateurService utilisateurService;
    private UtilisateurRepresentationMapper utilisateurRepresentationMapper;

    @Autowired
    public UtilisateurController(UtilisateurService utilisateurService, UtilisateurRepresentationMapper utilisateurRepresentationMapper) {
        this.utilisateurService = utilisateurService;
        this.utilisateurRepresentationMapper = utilisateurRepresentationMapper;
    }

    @GetMapping
    List<UtilisateurRepresentation> getAllUtilisateurs() {
        return this.utilisateurService.getAllUtilisateurs().stream()
                .map(this.utilisateurRepresentationMapper::mapToDisplayableUtilisateur)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    ResponseEntity<UtilisateurRepresentation> getOneUtilisateur(@PathVariable("id") String id) {
        Optional<Utilisateur> utilisateur = this.utilisateurService.getOneUtilisateur(id);
        return utilisateur
                .map(this.utilisateurRepresentationMapper::mapToDisplayableUtilisateur)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<UtilisateurRepresentation> deleteOneUtilisateur(@PathVariable("id") String id) {
       this.utilisateurService.deleteOneUtilisateur(id);
       return ResponseEntity.noContent().build();
    }

    @PostMapping
    public UtilisateurRepresentation createUtilisateur(@RequestBody NewUtilisateurRepresentation body) {
        return utilisateurRepresentationMapper.mapToDisplayableUtilisateur(this.utilisateurService.createOneUtilisateur(body));
    }

    @PutMapping("/{id}")
    public ResponseEntity<UtilisateurRepresentation> modifyUtilisateur(@PathVariable("id") String id ,
                                                               @RequestBody UtilisateurRepresentation body) {
        this.utilisateurService.modifyOneUtilisateur(id, body);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
