package com.blogexamfinal.blogexamfinalback.domain;

import javax.persistence.*;

@Entity
@Table(name = "utilisateurs")
public class Utilisateur {

    @Id
    @Column(name = "id_utilisateur")
    private String id;
    private String pseudo;
    private String email;
    // @OneToMany()
    // @JoinColumn(name = "id_article")
    // private List<Article> article;

    public Utilisateur(String id, String pseudo, String email)  {
        this.id = id;
        this.pseudo = pseudo;
        this.email = email;
      //  this.article = article;
    }

    public Utilisateur() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

   /* public List<Article> getArticle() {
        return article;
    }

    public void setArticle(List<Article> idArticle) {
        this.article = idArticle;
    }*/
}