package com.blogexamfinal.blogexamfinalback.domain;

import javax.persistence.*;

@Entity
@Table(name = "articles")
public class Article {

    @Id
    @Column(name = "id_article")
    private String id;
    private String titre;
    private String contenu;
    private String resume;
    @ManyToOne()
    @JoinColumn(name = "utilisateur")
    private Utilisateur utilisateur;
    @Enumerated(value = EnumType.STRING)
    private Categories categories;

    public Article(String id, String titre, String contenu, String resume, Utilisateur utilisateur, Categories categories) {
        this.id = id;
        this.titre = titre;
        this.contenu = contenu;
        this.resume = resume;
        this.utilisateur = utilisateur;
        this.categories = categories;
            }

    protected Article() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur idUtilisateur) {
        this.utilisateur = idUtilisateur;
    }

    public Categories getCategories() {
        return categories;
    }

    public void setCategories(Categories categories) {
        this.categories = categories;
    }
}
