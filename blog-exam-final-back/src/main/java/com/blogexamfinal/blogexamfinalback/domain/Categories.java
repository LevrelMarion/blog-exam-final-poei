package com.blogexamfinal.blogexamfinalback.domain;

public enum Categories {
    astuce,
    bonnesPratiques,
    bug,
    CSS3,
    google,
    hebergement,
    navigateur,
    php,
    referencement,
    repertoire,
    securite,
    serveur,
    coronavirus,
}
