package com.blogexamfinal.blogexamfinalback.repositories;

import com.blogexamfinal.blogexamfinalback.domain.Article;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends CrudRepository<Article, String> {

}
