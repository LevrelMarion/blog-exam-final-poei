package com.blogexamfinal.blogexamfinalback.repositories;

import com.blogexamfinal.blogexamfinalback.domain.Article;
import com.blogexamfinal.blogexamfinalback.domain.Utilisateur;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UtilisateurRepository extends CrudRepository<Utilisateur, String> {

}
